/*
 * Class: hyperdual
 * 
 * Implementation of a vector-mode version of hyper-dual numbers
 *
 * Written by: Jeffrey A. Fike
 * Stanford University, Department of Aeronautics and Astronautics
 * 
 * Copyright (c) 2006 Jeffrey A. Fike
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#ifndef _hyperdual_h
#define _hyperdual_h

#include <iostream>
#include <math.h>
using namespace std;

// HDSIZE is used to specify the number of variables to compute derivatives
// with respect to. 
#define HDSIZE 2

class hyperdual{
    double fun;
        double grad[HDSIZE];
        double hess[HDSIZE][HDSIZE];
public:
    //create new and set values
    hyperdual();
    hyperdual(double x1,int index);
    hyperdual(double x1);
    void setvalue(double x1,int index);

    //examine values
/*    void view(void);
    double function(void);
    double gradient(void);
    double hessian(void);
*/
    //basic manipulation
    //do not need to overload = operator?  fq=fq and fq=double seem to work
    
    hyperdual operator+ (hyperdual rhs);
    friend hyperdual operator+ (double lhs, hyperdual rhs);
    
    hyperdual operator- ();
    hyperdual operator- (hyperdual rhs);
    friend hyperdual operator- (double lhs, hyperdual rhs);

    hyperdual operator* (hyperdual rhs);
    friend hyperdual operator* (double lhs, hyperdual rhs);

    friend hyperdual operator/ (hyperdual lhs, hyperdual rhs);
    friend hyperdual operator/ (double lhs, hyperdual rhs);
    friend hyperdual operator/ (hyperdual lhs, double rhs);
    
    hyperdual& operator+= (hyperdual rhs);
    hyperdual& operator-= (hyperdual rhs);
    hyperdual& operator*= (hyperdual rhs);
    hyperdual& operator*= (double rhs);
    hyperdual& operator/= (double rhs);

    //math.h functions
    friend hyperdual pow (hyperdual x, double a);
    friend hyperdual exp(hyperdual x);
    friend hyperdual log(hyperdual x);
    friend hyperdual sin(hyperdual x);
    friend hyperdual cos(hyperdual x);
    friend hyperdual tan(hyperdual x);
    friend hyperdual asin(hyperdual x);
    //friend hyperdual acos(hyperdual x);
    friend hyperdual atan(hyperdual x);
    friend hyperdual sqrt(hyperdual x);
    friend hyperdual fabs(hyperdual x);

    //comparisons
    friend bool operator> (hyperdual lhs, hyperdual rhs);
    friend bool operator> (double lhs, hyperdual rhs);
    friend bool operator> (hyperdual lhs, double rhs);
    friend bool operator>= (hyperdual lhs, hyperdual rhs);
    friend bool operator>= (double lhs, hyperdual rhs);
    friend bool operator>= (hyperdual lhs, double rhs);
    friend bool operator< (hyperdual lhs, hyperdual rhs);
    friend bool operator< (double lhs, hyperdual rhs);
    friend bool operator< (hyperdual lhs, double rhs);
    friend bool operator<= (hyperdual lhs, hyperdual rhs);
    friend bool operator<= (double lhs, hyperdual rhs);
    friend bool operator<= (hyperdual lhs, double rhs);
    friend bool operator== (hyperdual lhs, hyperdual rhs);
    friend bool operator== (double lhs, hyperdual rhs);
    friend bool operator== (hyperdual lhs, double rhs);
    friend bool operator!= (hyperdual lhs, hyperdual rhs);
    friend bool operator!= (double lhs, hyperdual rhs);
    friend bool operator!= (hyperdual lhs, double rhs);

    friend ostream& operator<<(ostream& output, const hyperdual& rhs);

};

hyperdual::hyperdual()
{
    fun = 0.0;
        for (int i=0;i<HDSIZE;i++)
        {
          grad[i]= 0.0;
          for (int j=0;j<HDSIZE;j++)
          {
            hess[i][j] = 0.0;
          }
        }
}
hyperdual::hyperdual(double x1,int index)
{
    fun = x1;
        for (int i=0;i<HDSIZE;i++)
        {
          grad[i]= 0.0;
          for (int j=0;j<HDSIZE;j++)
          {
            hess[i][j] = 0.0;
          }
        }
        if (index < HDSIZE)
          grad[index] = 1.0;
        else
          cerr << "Error:  index to gradient array larger than HDSIZE.\n";
}
hyperdual::hyperdual(double x1)
{
    fun = x1;
        for (int i=0;i<HDSIZE;i++)
        {
          grad[i]= 0.0;
          for (int j=0;j<HDSIZE;j++)
          {
            hess[i][j] = 0.0;
          }
        }
}

void hyperdual::setvalue(double x1,int index)
{
    fun = x1;
        for (int i=0;i<HDSIZE;i++)
        {
          grad[i]= 0.0;
          for (int j=0;j<HDSIZE;j++)
          {
            hess[i][j] = 0.0;
          }
        }
        if (index < HDSIZE)
          grad[index] = 1.0;
        else
          cerr << "Error:  index to gradient array larger than HDSIZE.\n";
}
/*void hyperdual::view(void)
{
    printf("%g  +  %g i  +  %g j  +  %g ij\n",f0,f1,f2,f12);
}
*/
/*double hyperdual::realpart(void)
{
    return f0;
}
double hyperdual::ipart(void)
{
    return f1;
}
double hyperdual::jpart(void)
{
    return f2;
}
double hyperdual::ijpart(void)
{
    return f12;
}
*/
hyperdual hyperdual::operator+ (hyperdual rhs)
{
    hyperdual temp;
    temp.fun = fun + rhs.fun;
        for (int i=0;i<HDSIZE;i++)
        {
          temp.grad[i]= grad[i]+rhs.grad[i];
          for (int j=0;j<HDSIZE;j++)
          {
            temp.hess[i][j]= hess[i][j]+rhs.hess[i][j];
          }
        }
    return temp;
}

hyperdual operator+ (double lhs, hyperdual rhs)
{
    hyperdual temp;
    temp.fun = lhs + rhs.fun;
        for (int i=0;i<HDSIZE;i++)
        {
          temp.grad[i]= rhs.grad[i];
          for (int j=0;j<HDSIZE;j++)
          {
            temp.hess[i][j]= rhs.hess[i][j];
          }
        }
    return temp;
}


hyperdual hyperdual::operator- ()
{
    hyperdual temp;
    temp.fun = -fun;
        for (int i=0;i<HDSIZE;i++)
        {
          temp.grad[i]= -grad[i];
          for (int j=0;j<HDSIZE;j++)
          {
            temp.hess[i][j]= -hess[i][j];
          }
        }
    return temp;
}

hyperdual hyperdual::operator- (hyperdual rhs)
{
    hyperdual temp;
    temp.fun = fun - rhs.fun;
        for (int i=0;i<HDSIZE;i++)
        {
          temp.grad[i]= grad[i]-rhs.grad[i];
          for (int j=0;j<HDSIZE;j++)
          {
            temp.hess[i][j]= hess[i][j]-rhs.hess[i][j];
          }
        }
    return temp;
}
hyperdual operator- (double lhs, hyperdual rhs)
{
    hyperdual temp;
    temp.fun = lhs - rhs.fun;
        for (int i=0;i<HDSIZE;i++)
        {
          temp.grad[i]= -rhs.grad[i];
          for (int j=0;j<HDSIZE;j++)
          {
            temp.hess[i][j]= -rhs.hess[i][j];
          }
        }
    return temp;
}

hyperdual hyperdual::operator* (hyperdual rhs)
{
    hyperdual temp;
    temp.fun = fun*rhs.fun;
        for (int i=0;i<HDSIZE;i++)
        {
          temp.grad[i]= fun*rhs.grad[i] + rhs.fun*grad[i];
          for (int j=0;j<HDSIZE;j++)
          {
            temp.hess[i][j]= fun*rhs.hess[i][j] + rhs.fun*hess[i][j] + grad[i]*rhs.grad[j] + grad[j]*rhs.grad[i];
          }
        }
    return temp;
}

hyperdual operator* (double lhs, hyperdual rhs)
{
    hyperdual temp;
    temp.fun = lhs*rhs.fun;
        for (int i=0;i<HDSIZE;i++)
        {
          temp.grad[i]= lhs*rhs.grad[i];
          for (int j=0;j<HDSIZE;j++)
          {
            temp.hess[i][j]= lhs*rhs.hess[i][j];
          }
        }
    return temp;
}
hyperdual operator/ (hyperdual lhs, hyperdual rhs)
{
    hyperdual temp,inv;
    inv = pow(rhs,-1);
    temp = lhs*inv;
    return temp;
}

hyperdual operator/ (double lhs, hyperdual rhs)
{
    hyperdual temp,inv;
    inv = pow(rhs,-1);
    temp = lhs*inv;
    return temp;
}

hyperdual operator/ (hyperdual lhs, double rhs)
{
    hyperdual temp;
    double inv;
    inv = 1.0/rhs;
    temp = inv*lhs;
    return temp;
}

hyperdual& hyperdual::operator+= (hyperdual rhs)
{
    fun += rhs.fun;
        for (int i=0;i<HDSIZE;i++)
        {
          grad[i]+=rhs.grad[i];
          for (int j=0;j<HDSIZE;j++)
          {
            hess[i][j]+=rhs.hess[i][j];
          }
        }
    return *this;
}
hyperdual& hyperdual::operator-= (hyperdual rhs)
{
    fun -= rhs.fun;
        for (int i=0;i<HDSIZE;i++)
        {
          grad[i]-=rhs.grad[i];
          for (int j=0;j<HDSIZE;j++)
          {
            hess[i][j]-=rhs.hess[i][j];
          }
        }
    return *this;
}
hyperdual& hyperdual::operator*= (hyperdual rhs)
{
    hyperdual temp;
    temp.fun = fun*rhs.fun;
        for (int i=0;i<HDSIZE;i++)
        {
          temp.grad[i]= fun*rhs.grad[i] + rhs.fun*grad[i];
          for (int j=0;j<HDSIZE;j++)
          {
            temp.hess[i][j]= fun*rhs.hess[i][j] + rhs.fun*hess[i][j] + grad[i]*rhs.grad[j] + grad[j]*rhs.grad[i];
          }
        }
    fun = temp.fun;
        for (int i=0;i<HDSIZE;i++)
        {
          grad[i] = temp.grad[i];
          for (int j=0;j<HDSIZE;j++)
          {
            hess[i][j] = temp.hess[i][j];
          }
        }
    return *this;
}

hyperdual& hyperdual::operator*= (double rhs)
{
    hyperdual temp;
    temp.fun = fun*rhs;
        for (int i=0;i<HDSIZE;i++)
        {
          temp.grad[i]= rhs*grad[i];
          for (int j=0;j<HDSIZE;j++)
          {
            temp.hess[i][j]= rhs*hess[i][j];
          }
        }
    fun = temp.fun;
        for (int i=0;i<HDSIZE;i++)
        {
          grad[i] = temp.grad[i];
          for (int j=0;j<HDSIZE;j++)
          {
            hess[i][j] = temp.hess[i][j];
          }
        }
    return *this;
}
hyperdual& hyperdual::operator/= (double rhs)
{
    hyperdual temp;
    temp.fun = fun/rhs;
        for (int i=0;i<HDSIZE;i++)
        {
          temp.grad[i]= grad[i]/rhs;
          for (int j=0;j<HDSIZE;j++)
          {
            temp.hess[i][j]= hess[i][j]/rhs;
          }
        }
    fun = temp.fun;
        for (int i=0;i<HDSIZE;i++)
        {
          grad[i] = temp.grad[i];
          for (int j=0;j<HDSIZE;j++)
          {
            hess[i][j] = temp.hess[i][j];
          }
        }
    return *this;
}

hyperdual pow (hyperdual x, double a)
{
    hyperdual temp;
    double deriv,xval,tol;
    xval = x.fun;
    tol = 1e-15;
    if (fabs(xval) < tol)
    {
        if (xval >= 0)
            xval = tol;
        if (xval < 0)
            xval = -tol;
    }
    deriv = a*pow(xval,(a-1));
    temp.fun = pow(x.fun,a);  //Use actual x value, only use tol for derivs
        for (int i=0;i<HDSIZE;i++)
        {
          temp.grad[i]= x.grad[i]*deriv;
          for (int j=0;j<HDSIZE;j++)
          {
            temp.hess[i][j]= deriv*x.hess[i][j] + a*(a-1)*x.grad[i]*x.grad[j]*pow(xval,a-2);
          }
        }
    return temp;
}
hyperdual exp(hyperdual x)
{
    hyperdual temp;
    double deriv;
    deriv = exp(x.fun);
    temp.fun = deriv;
        for (int i=0;i<HDSIZE;i++)
        {
          temp.grad[i]= x.grad[i]*deriv;
          for (int j=0;j<HDSIZE;j++)
          {
            temp.hess[i][j]= deriv*(x.hess[i][j] + x.grad[i]*x.grad[j]);
          }
        }
    return temp;
}
hyperdual log(hyperdual x)
{
    hyperdual temp;
    temp.fun = log(x.fun);
        for (int i=0;i<HDSIZE;i++)
        {
          temp.grad[i]= x.grad[i]/x.fun;
          for (int j=0;j<HDSIZE;j++)
          {
            temp.hess[i][j]= x.hess[i][j]/x.fun - (x.grad[i]*x.grad[j])/(x.fun*x.fun);
          }
        }
    return temp;
}

hyperdual sin(hyperdual x)
{
    hyperdual temp;
    double funval,deriv;
    funval = sin(x.fun);
    deriv = cos(x.fun);
    temp.fun = funval;
        for (int i=0;i<HDSIZE;i++)
        {
          temp.grad[i]= deriv*x.grad[i];
          for (int j=0;j<HDSIZE;j++)
          {
            temp.hess[i][j]= deriv*x.hess[i][j] - funval*x.grad[i]*x.grad[j];
          }
        }
    return temp;
}

hyperdual cos(hyperdual x)
{
    hyperdual temp;
    double funval,deriv;
    funval = cos(x.fun);
    deriv = -sin(x.fun);
    temp.fun = funval;
        for (int i=0;i<HDSIZE;i++)
        {
          temp.grad[i]= deriv*x.grad[i];
          for (int j=0;j<HDSIZE;j++)
          {
            temp.hess[i][j]= deriv*x.hess[i][j] - funval*x.grad[i]*x.grad[j];
          }
        }
    return temp;
}
hyperdual tan(hyperdual x)
{
    hyperdual temp;
    double funval,deriv;
    funval = tan(x.fun);
    deriv  = funval*funval + 1.0;
    temp.fun = funval;
        for (int i=0;i<HDSIZE;i++)
        {
          temp.grad[i]= deriv*x.grad[i];
          for (int j=0;j<HDSIZE;j++)
          {
            temp.hess[i][j]= deriv*x.hess[i][j] + x.grad[i]*x.grad[j]*(2*funval*deriv);
          }
        }
    return temp;
}

hyperdual asin(hyperdual x)
{
    hyperdual temp;
    double funval,deriv1,deriv;
    funval = asin(x.fun);
    deriv1 = 1.0-x.fun*x.fun;
    deriv = 1.0/sqrt(deriv1);
    temp.fun = funval;
        for (int i=0;i<HDSIZE;i++)
        {
          temp.grad[i]= deriv*x.grad[i];
          for (int j=0;j<HDSIZE;j++)
          {
            temp.hess[i][j]= deriv*x.hess[i][j] + x.grad[i]*x.grad[j]*(x.fun*pow(deriv1,-1.5));
          }
        }
    return temp;
}
hyperdual atan(hyperdual x)
{
    hyperdual temp;
    double funval,deriv1,deriv;
    funval = atan(x.fun);
    deriv1 = 1.0+x.fun*x.fun;
    deriv = 1.0/deriv1;
    temp.fun = funval;
        for (int i=0;i<HDSIZE;i++)
        {
          temp.grad[i]= deriv*x.grad[i];
          for (int j=0;j<HDSIZE;j++)
          {
            temp.hess[i][j]= deriv*x.hess[i][j] + x.grad[i]*x.grad[j]*(-2*x.fun/(deriv1*deriv1));
          }
        }
    return temp;
}

hyperdual sqrt(hyperdual x)
{
    return pow(x,0.5);
}

hyperdual fabs(hyperdual x)
{
    hyperdual temp;
    if (x < 0.0)
        temp = -x;
    else
        temp = x;
    return temp;
}

bool operator> (hyperdual lhs, hyperdual rhs)
{
    return (lhs.fun > rhs.fun);
}
bool operator> (double lhs, hyperdual rhs)
{
    return (lhs > rhs.fun);
}
bool operator> (hyperdual lhs, double rhs)
{
    return (lhs.fun > rhs);
}
bool operator>= (hyperdual lhs, hyperdual rhs)
{
    return (lhs.fun >= rhs.fun);
}
bool operator>= (double lhs, hyperdual rhs)
{
    return (lhs >= rhs.fun);
}
bool operator>= (hyperdual lhs, double rhs)
{
    return (lhs.fun >= rhs);
}
bool operator< (hyperdual lhs, hyperdual rhs)
{
    return (lhs.fun < rhs.fun);
}
bool operator< (double lhs, hyperdual rhs)
{
    return (lhs < rhs.fun);
}
bool operator< (hyperdual lhs, double rhs)
{
    return (lhs.fun < rhs);
}
bool operator<= (hyperdual lhs, hyperdual rhs)
{
    return (lhs.fun <= rhs.fun);
}
bool operator<= (double lhs, hyperdual rhs)
{
    return (lhs <= rhs.fun);
}
bool operator<= (hyperdual lhs, double rhs)
{
    return (lhs.fun <= rhs);
}
bool operator== (hyperdual lhs, hyperdual rhs)
{
    return (lhs.fun == rhs.fun);
}
bool operator== (double lhs, hyperdual rhs)
{
    return (lhs == rhs.fun);
}
bool operator== (hyperdual lhs, double rhs)
{
    return (lhs.fun == rhs);
}
bool operator!= (hyperdual lhs, hyperdual rhs)
{
    return (lhs.fun != rhs.fun);
}
bool operator!= (double lhs, hyperdual rhs)
{
    return (lhs != rhs.fun);
}
bool operator!= (hyperdual lhs, double rhs)
{
    return (lhs.fun != rhs);
}

/*ostream& operator<<(ostream& output, const hyperdual& rhs)
{
        output << "Function = \t" << rhs.fun << "\n";
        output << "\n";
        output << "Gradient = \t";
        for (int i=0;i<HDSIZE;i++)
        {
          output << rhs.grad[i] << "\n\t\t";
        }
        output << "\n";
        output << "Hessian = \t";
        for (int i=0;i<HDSIZE;i++)
        {
          for (int j=0;j<HDSIZE;j++)
          {
            output << rhs.hess[i][j] << "\t";
          }
          output << "\n\t\t";
        }
        return output;
}
*/
ostream& operator<<(ostream& output, const hyperdual& rhs)
{
        output << "Function = \t" << rhs.fun << "\t";
        output << "Gradient = \t";
        output << rhs.grad[0] << "\t";
        output << "Hessian = \t";
        for (int j=0;j<HDSIZE;j++)
            output << rhs.hess[0][j] << "\t";
        for (int i=1;i<HDSIZE;i++)
        {
            output << "\n\t\t\t\t\t";
            output << rhs.grad[i] << "\t";
            output << "\t\t";
            for (int j=0;j<HDSIZE;j++)
                output << rhs.hess[i][j] << "\t";
        }
        return output;
}


#endif

