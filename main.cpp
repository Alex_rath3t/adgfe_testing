
#include <math.h>
#include <vector>
#include "hyperdual.h"
#include "Packages/CoDiPack/include/codi.hpp"

// Eigen includes
#include "Packages/Eigen/Core"
using namespace Eigen;

#include <iostream>
using namespace std;
// autodiff include
#include "Packages/autodiff/forward.hpp"
#include "Packages/autodiff/forward/eigen.hpp"

using dual2nd = autodiff::forward::HigherOrderDual<2>;

//#include "autodiff/reverse.hpp"
//#include "autodiff/reverse/eigen.hpp"

using namespace autodiff;

#include <chrono>
#include <unistd.h>


template<typename Real>
void funcfCP(const Real* x, Real* y) {
    y[0] = exp(x[0])/sqrt((sin(x[0])*sin(x[0])*sin(x[0]))+cos(x[0])*cos(x[0])*cos(x[0]))*exp(sin(pow(x[1],5)));
}

template<typename Real>
void funcfCPVector(const Real* x, Real* y,int size) {
    y[0] = 0.0;
    for(int i=0; i< size; ++i)
        y[0] += x[i];
}

double function_f (double x,double y)
{
    return exp(x)/sqrt((sin(x)*sin(x)*sin(x))+cos(x)*cos(x)*cos(x))*exp(sin(pow(y,5)));
}

dual funcf(VectorXdual& x)
{
    return exp(x[0])/sqrt((sin(x[0])*sin(x[0])*sin(x[0]))+cos(x[0])*cos(x[0])*cos(x[0]))*exp(sin(pow(x[1],5)));
}

//dual funcf(VectorXdual2d& x)
//{
//    return exp(x[0])/sqrt((sin(x[0])*sin(x[0])*sin(x[0]))+cos(x[0])*cos(x[0])*cos(x[0]))*exp(sin(pow(x[1],5)));
//}

//
//var funcf(const VectorXvar& x)
//{
//    return exp(x[0])/sqrt((sin(x[0])*sin(x[0])*sin(x[0]))+cos(x[0])*cos(x[0])*cos(x[0]))*exp(sin(pow(x[1],5)));
//}

//vector<double> function_dist (vector<double> x,vector<double> y,double xi)
//{
//    double N1=1/2*(1-xi);
//    double N2=1/2*(1+xi);
//
//    vector<double> qsol(3);
//qsol=x;
//double alpha_prime = -2*acos()
//
//    return
//}

complex<double> function_f (complex<double> x,complex<double> y)
{
    return exp(x)/sqrt((sin(x)*sin(x)*sin(x))+cos(x)*cos(x)*cos(x))*exp(sin(pow(y,5)));
}

hyperdual function_f (hyperdual x,hyperdual y)
{
    return exp(x)/sqrt((sin(x)*sin(x)*sin(x))+cos(x)*cos(x)*cos(x))*exp(sin(pow(y,5)));
}



vector<double> forward_1(double x,double y,double h)
{
    vector<double> grad(2);
    double func_value =function_f(x,y);
    grad[0]=(function_f(x+h,y)-func_value)/h;
    grad[1]=(function_f(x,y+h)-func_value)/h;
    return grad;
}


vector<double> forward_1_hess(double x,double y,double h)
{
    vector<double>  hess(4);
    double func_value =function_f(x,y);
    hess[0]=(function_f(x+2*h,y)-function_f(x+h,y)-function_f(x+h,y))/(h*h);
    hess[1]=(function_f(x+h,y+h)-function_f(x+h,y)-function_f(x,y+h))/(h*h);
    hess[2]=(function_f(x+h,y+h)-function_f(x,y+h)-function_f(x+h,y))/(h*h);
    hess[3]=(function_f(x,y+2*h)-function_f(x,y+h)-function_f(x,y+h))/(h*h);

    return hess;
}


vector<double> central_1(double x,double y,double h)  // Ableitung der Funktion f(x) = x^2 an der Stelle x = 2
{
    vector<double> grad(2);
    grad[0]=(function_f(x+h,y)-function_f(x-h,y))/(2*h);
    grad[1]=(function_f(x,y+h)-function_f(x,y-h))/(2*h);
    return grad;
}


vector<double> complex_1(double x,double y,double h)
{
    complex<double> x_c (x,0.0);
    complex<double> h_c(0.0,h);
    complex<double> y_c (y,0.0);

    vector<double> grad(2);
    grad[0]=imag(function_f(x_c+h_c,y_c))/imag(h_c);
    grad[1]=imag(function_f(x_c,y_c+h_c))/imag(h_c);

    return grad;
}


vector<double> hyperdual_1(double x,double y,double h)
{
    hyperdual x_c (x,0.0,0.0,0.0);
    hyperdual h_c1(0.0,h,0.0,0.0);
    hyperdual h_c2(0.0,0.0,h,0.0);
    hyperdual y_c (y,0.0,0.0,0.0);

    vector<double> gradAndHess(6);
    hyperdual func_value1 =function_f(x_c+h_c1,y_c+h_c2);
    gradAndHess[0]=func_value1.eps1()/h;
    gradAndHess[1]=func_value1.eps2()/h;

    gradAndHess[3]=func_value1.eps1eps2()/(h*h);

    func_value1 =function_f(x_c+h_c2,y_c+h_c1);

    gradAndHess[4]=func_value1.eps1eps2()/(h*h);

    func_value1 =function_f(x_c+h_c1+h_c2,y_c);
    gradAndHess[2]=func_value1.eps1eps2()/(h*h);
    func_value1 =function_f(x_c,y_c+h_c1+h_c2);
    gradAndHess[5]=func_value1.eps1eps2()/(h*h);

    return gradAndHess;
}

void finite_difference_forward(VectorXd xe)
{

}

int main()
{

    double x=2.0;
    double y=-2.0;
    int problemSize = 2;
    VectorXd xeval = VectorXd::Random(problemSize);

    int cycles= 100000;
    vector<double> grad(2);
    vector<double> gradHess(6);
    vector<double> Hess(4);
    cout << "Exact: "<< endl;
    cout<< 10.87785034106638 <<" "<< 344.5895680256127 <<" "<< 47.56336829952129 <<" "<< 725.9645493289022<<" "<< 725.9645493289022<<" "<< 40530.05716243066<< endl;
    cout << "FD: "<< endl;
    auto start = chrono::steady_clock::now();
    for (int j = 0; j < cycles; ++j) {
        double h = 1e-8;
        grad = forward_1(x, y, h);
        Hess = forward_1_hess(x, y, h);
        //cout << grad[0] << " " << grad[1] << " " << Hess[0] << " " << Hess[1] << " " << Hess[2] << " " << Hess[3]
        //    << endl;

    }
    auto end = chrono::steady_clock::now();
    cout << "Elapsed time in seconds :  "
         << chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
         << " ms \n";
//    cout << "CD: "<< endl;
//    for (int i = 0; i < 5; ++i) {
//        double h=pow(10,-i);
//        grad=central_1(x,y,h);
//        cout<< grad[0]<< " "<< grad[1]<<endl;
//    }
//    cout << "CS: "<< endl;
//    for (int i = 0; i < 5; ++i) {
//        double h=pow(10,-i);
//        grad=complex_1(x,y,h);
//        cout<< grad[0]<< " "<< grad[1]<<endl;
//    }

    cout << "HyperDual: "<< endl;
    auto start1 = chrono::steady_clock::now();
    for (int j = 0; j < cycles; ++j) {
        double h = 1e-8;
        gradHess = hyperdual_1(x, y, h);
//            cout << gradHess[0] << " " << gradHess[1] << " " << gradHess[2] << " " << gradHess[3] << " " << gradHess[4]
//                 << " " << gradHess[5] << endl;
    }
    auto end1 = chrono::steady_clock::now();
    cout << "Elapsed time in seconds : "
         << chrono::duration_cast<std::chrono::milliseconds>(end1 - start1).count()
         << " ms \n";



//    cout << "AutoDiff Reverse: "<< endl;
//    auto start2 = chrono::steady_clock::now();
//    VectorXvar xd(2);
//    xd << x,y;
//    var yd = funcf(xd);
//    for (int j = 0; j < cycles; ++j) {
//
//        for (int i = 0; i < 5; ++i) {
//                             // the input vector x with 5 variables
//
//
//
//                                // the output variable y
//
//        VectorXd dydx = gradient(yd, xd);
//        MatrixXd dydxdx = hessian(yd, xd);
//
//        //cout  << dydx[0]<<" "<<dydx[1] <<" "<<dydxdx(0,0)<<" "<<dydxdx(1,0)<<" "<<dydxdx(0,1)<<" "<<dydxdx(1,1)<< endl;
//    }
//    }
//    auto end2 = chrono::steady_clock::now();
//    cout << "Elapsed time in seconds : "
//         << chrono::duration_cast<chrono::seconds>(end2 - start2).count()
//         << " sec \n";

    cout << "AutoDiff Forward: "<< endl;
    VectorXdual xdd(2);
    dual u;
    auto start3 = chrono::steady_clock::now();

    for (int j = 0; j < cycles; ++j) {


        // the input vector x with 5 variables


        xdd << x,y;


        VectorXd g = autodiff::forward::gradient(funcf, wrt(xdd), at(xdd),u);



        //cout  << dydx[0]<<" "<<dydx[1] <<" "<<dydxdx(0,0)<<" "<<dydxdx(1,0)<<" "<<dydxdx(0,1)<<" "<<dydxdx(1,1)<< endl;

    }
    auto end3 = chrono::steady_clock::now();
    cout << "Elapsed time in seconds : "
         << chrono::duration_cast<std::chrono::milliseconds>(end3 - start3).count()
         << " ms \n";



    auto start4 = chrono::steady_clock::now();

    cout << "CoDiPack Forward: "<< endl;

    codi::RealForwardVec<2> xCP[2];
    codi::RealForwardVec<2> yCP[1];
    xCP[0] = 2.0;
    xCP[1] = -2.0;

    for(size_t i = 0; i < 5; ++i) {
        xCP[i].gradient()[i] = 1.0;
    }

    double jacobi[2][1];
    for (int j = 0; j < cycles; ++j) {
        // Forward vector mode
        funcfCP(xCP, yCP);
        for(size_t i = 0; i < 2; ++i) {
            jacobi[i][0] = yCP[0].getGradient()[i];
        }
        //std::cout << "Forward vector mode:" << std::endl;
        //std::cout << "f(1 .. 5) = (" << yCP[0] << ", " << ")" << std::endl;
        //for(size_t i = 0; i < 2; ++i) {
        //  std::cout << "df/dx_" << (i + 1) << " (1 .. 5) = (" << jacobi[i][0]  << ")" << std::endl;
        //}
    }
    auto end4 = chrono::steady_clock::now();
    cout << "Elapsed time in seconds : "
         << chrono::duration_cast<std::chrono::milliseconds>(end4 - start4).count()
         << " ms \n";

    cout << "CoDiPack Reverse: "<< endl;
    double jacobiR[2][1];
    auto start5 = chrono::steady_clock::now();
    // Reverse vector mode
    codi::RealReverseVec<2> xR[2];
    codi::RealReverseVec<2> yR[2];
    xR[0] = 2.0;
    xR[1] = -2.0;

    codi::RealReverseVec<2>::TapeType& tape = codi::RealReverseVec<2>::getGlobalTape();
    tape.setActive();
    for(size_t i = 0; i < 2; ++i) {
        tape.registerInput(xR[i]);
    }
    funcfCP(xR,yR);
    tape.registerOutput(yR[0]);
    tape.setPassive();
    for (int j = 0; j < cycles; ++j) {


    yR[0].gradient()[0] = 1.0;
    yR[1].gradient()[1] = 1.0;
    tape.evaluate();

    for(size_t i = 0; i < 2; ++i) {
        jacobiR[i][0] = xR[i].getGradient()[0];
    }
}
    cout<<jacobiR[0][0]<<" "<<jacobiR[1][0]<<endl;
auto end5 = chrono::steady_clock::now();
cout << "Elapsed time in seconds : "
<< chrono::duration_cast<std::chrono::milliseconds>(end5 - start5).count()
<< " ms \n";


    return 0;
}


void print()
{

}